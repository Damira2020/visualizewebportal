from app import app, db
from app.models import Entry
from app.forms import EntryForm
from flask import render_template, redirect, url_for
import os

def initDB():
    if not os.path.isfile("./app/database.db"):
        db.create_all()
        entry1 = Entry(text="entry1 test")
        entry2 = Entry(text="entry2 test")
        db.session.add(entry1)
        db.session.add(entry2)
        db.session.commit()
       

@app.route("/")
def home():
    initDB()
    entries = Entry.query.all()

    return render_template("home.html", entries=entries)

@app.route("/new_entry", methods=["GET", "POST"])
def new_entry():
    form = EntryForm()
    if form.validate_on_submit():
        entry = Entry(text=form.text.data)
        db.session.add(entry)
        db.session.commit()
        return redirect(url_for("home"))
    return render_template("new_entry.html", form=form, headerText="Add new entry")

@app.route("/update/<int:entry_id>", methods=["GET", "POST"])
def update_post(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    
    form = EntryForm()
    if form.validate_on_submit():
        entry.text = form.text.data
        db.session.commit()
        return redirect(url_for("home"))
    
    form.text.data = entry.text
    return render_template("new_entry.html", form=form, headerText="Update Entry")

@app.route("/delete/<int:entry_id>", methods=["POST"])
def delete_post(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    
    db.session.delete(entry)
    db.session.commit()
    
    return redirect(url_for("home"))
    