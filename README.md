# Visualize Web Portal

This project was made with the intention of creating a repository where we can experiment with different technologies that we will possibly need to know later on.

## Getting Started

### Prerequisites

To work with this project, users will need a linux machine with docker and docker composed installed. This is a python 3 project using the flask framework, so users will also need the required tools to make those work. Once the linux machine is open, run the following commands to install the prerequisites :
```
$ sudo apt-get update

$ sudo apt-get install docker.io

$ sudo apt install curl
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

$ sudo apt install python3
$ sudo apt install python3-pip

$ pip3 install flask
```

To run the application, all that should be needed is to navigate to the project repository and to run the following command : 
```
$ sudo docker-compose up
```

Right now, the application is really bare-bones, but we will keep adding new stuff as we go along. If something doesn't work as intended or is missing, feel free to fix it or let someone know!

## Repository content

### Web portal
The web portal is located under folder "webPortal". It will run under http://localhost:5000

### Shiny server
The shiny server is located under folder "shiny". This is the same image used by project 22 (https://github.com/StatCan/shiny).The shiny server can be accessed on http://localhost:3838.

A basic shiny report can be found under http://localhost:3838/example-app/ You can add shiny reports  by creating a folder for each new report under "moutpoints/apps". Note that it takes around 10 minutes to build the initial image.